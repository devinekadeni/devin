<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'unit'], function () {
    Route::get('/getUnit', function(){
        $unitList = DB::table('unit')->get();
        return response()->json($unitList,200);
    });
    Route::post('/createNewUnit','ProductController@CreateUnit');
    Route::post('/deleteUnit', 'ProductController@DeleteUnit');
});
