<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UnitRumah;

class ProductController extends Controller
{
    function CreateUnit(Request $request){

        DB::beginTransaction();
        try{
            $newUnit = new UnitRumah;

            $newUnit->kavling = $request->input('kavling');
            $newUnit->blok = $request->input('blok');
            $newUnit->no_rumah = $request->input('no_rumah');
            $newUnit->harga_rumah = $request->input('harga_rumah');
            $newUnit->luas_tanah = $request->input('luas_tanah');
            $newUnit->luas_bangunan = $request->input('luas_bangunan');

            $newUnit->save();

            DB::commit();            
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(["message"=> $e->getMessage()], 500);
        }
    }
            
    function DeleteUnit(Request $request){

        DB::beginTransaction();
        try{
            $idUnit = $request->input('id');

            DB::delete('delete from unit where id = ?', [$idUnit]);

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(["message"=> $e->getMessage()], 500);
        }   
    }
}
